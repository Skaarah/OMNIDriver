#include "OverlaySource.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	for(int i = 0; i < argc; ++i)
	{
		std::string currArgument = std::string(argv[i]);
		if(currArgument == "-install")
		{

			return 0;
		}
	}
	QApplication a(argc, argv);
	OMNIOverlay w;
	w.show();
	return a.exec();
}
