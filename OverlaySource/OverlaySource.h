#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_OverlaySource.h"

class OMNIOverlay : public QMainWindow
{
	Q_OBJECT

	public:
	OMNIOverlay(QWidget *parent = Q_NULLPTR);

	private:
	Ui::OverlaySourceClass ui;

	private slots:
	void InstallDriver();
	void UninstallDriver();
};
