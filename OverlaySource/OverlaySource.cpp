#include "OverlaySource.h"
#include "..\DriverSource\OmniLog.h"
#include <nlohmann\json.hpp>
#include <fstream>
#include <QtWidgets\qmessagebox.h>
#include <algorithm>
#include <Windows.h>
#include <locale>
#include <codecvt>
#include <filesystem>
#include <qexception.h>

using json = nlohmann::json;

OMNIOverlay::OMNIOverlay(QWidget *parent) : QMainWindow(parent)
{
	ui.setupUi(this);

	connect(ui.installDriverButton, SIGNAL(clicked()), this, SLOT(InstallDriver()));

}

inline bool FileExists(const std::string& name)
{
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}
inline std::string BoolToString(bool b)
{
	return b ? "true" : "false";
}

void Alert(const std::string& title, const std::string& message)
{
	QMessageBox msgBox;
	msgBox.setText(title.c_str());
	msgBox.setInformativeText(message.c_str());
	msgBox.setStandardButtons(QMessageBox::Ok);
	int ret = msgBox.exec();
}

inline std::string WStringToString(const std::wstring& from)
{
	//setup converter
	using convert_type = std::codecvt_utf8<wchar_t>;
	std::wstring_convert<convert_type, wchar_t> converter;

	//use converter (.to_bytes: wstr->str, .from_bytes: str->wstr)
	return converter.to_bytes(from);
}

void OMNIOverlay::InstallDriver()
{
	try
	{
		std::string vrPathsPath = OmniLog::Format("%s%s", getenv("localappdata"), "\\openvr\\openvrpaths.vrpath");
		if(FileExists(vrPathsPath))
		{
			std::ifstream vrPathsFile(vrPathsPath);

			json configJson;
			vrPathsFile >> configJson;

			auto driverList = configJson["external_drivers"];
			if(std::all_of(driverList.begin(), driverList.end(), [](auto driver)
			{
				return driver.get<std::string>().find("OpenOmni") == std::string::npos;
			}))
			{
				TCHAR pwd[MAX_PATH];
				GetCurrentDirectory(MAX_PATH, pwd);
				std::experimental::filesystem::path currentPath(pwd);
				std::experimental::filesystem::path manifestPath;
				auto end = currentPath.end();
				end; //path in windows api doesn't normalize so it wasn't resolving ../ so this is the only other easy option
				for(auto iter = currentPath.begin(); iter != end; ++iter)
				{
					manifestPath.append(*iter);
				}
				configJson["external_drivers"].push_back(manifestPath.string());

				std::ofstream output(vrPathsPath);
				output << std::setw(4) << configJson << std::endl;
			}
		}
		else
		{
			Alert("Error: OpenVR Not Installed", OmniLog::Format("OpenVR is likely not installed.\nPlease make sure to run SteamVR at least once.\nConfig file should be located at:\n%s", vrPathsPath.c_str()));
		}
	}
	catch(QException e)
	{
		Alert("Unhandled Error", e.what());
	}
}


void OMNIOverlay::UninstallDriver()
{
	std::string configPath = getenv("localappdata");
	configPath += "\\openvr\\";
	if(FileExists(configPath + "openvrpaths.vrpath"))
	{
		std::ifstream configFile(configPath + "openvrpaths.vrpath");
		json configJson;
		configFile >> configJson;

		auto driverList = configJson["external_drivers"];
		if(std::all_of(driverList.begin(), driverList.end(), [](auto driver)
		{
			return driver.get<std::string>().find("OpenOmni") == std::string::npos;
		}))
		{
			TCHAR pwd[MAX_PATH];
			GetCurrentDirectory(MAX_PATH, pwd);
			std::experimental::filesystem::path currentPath(pwd);
			std::experimental::filesystem::path manifestPath;
			auto end = currentPath.end();
			----end; //path in windows api doesn't normalize so it wasn't resolving ../ so this is the only other easy option
			for(auto iter = currentPath.begin(); iter != end; ++iter)
			{
				manifestPath.append(*iter);
			}
			configJson["external_drivers"].push_back(manifestPath.string());

			std::ofstream output(configPath + "openvrpaths.vrpath");
			output << std::setw(4) << configJson << std::endl;
		}
	}
	else
	{
		Alert("Error: OpenVR Not Installed", OmniLog::Format("OpenVR is likely not installed.\nPlease make sure to run SteamVR at least once.\nConfig file should be located at:\n%s", configPath.c_str()));
	}
}
