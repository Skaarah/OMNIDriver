#include "OmniDriver.h"
#include <openvr_driver.h>


OmniServerProvider server;
OmniWatchdogProvider client;

#define HMD_DLL_EXPORT extern "C" __declspec( dllexport )

HMD_DLL_EXPORT
void *HmdDriverFactory(const char *pInterfaceName, int *pReturnCode)
{
	if(strcmp(vr::IServerTrackedDeviceProvider_Version, pInterfaceName) == 0)
	{
		return &server;
	}
	if(strcmp(vr::IVRWatchdogProvider_Version, pInterfaceName) == 0)
	{
		return &client;
	}

	if(pReturnCode)
		*pReturnCode = vr::VRInitError_Init_InterfaceNotFound;

	return nullptr;
}