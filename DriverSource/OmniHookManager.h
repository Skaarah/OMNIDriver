#include "openvr_driver.h"
#include "subhook.h"
#include <map>
#include <vector>
#include "Action.h"
#include <exception>
#include <share.h>
#include <memory>
#include "OmniLog.h"

#pragma once
class OmniHookException : public std::exception
{
	private:
	std::string funcName;
	public:
	OmniHookException(const char* name)
	{
		funcName = std::string(name);
	}
	virtual const char* what() const throw()
	{
		return OmniLog::Format("Omni Hook %s Failed to install", funcName).c_str();
	}
};

class ControllerVTableInfo
{
	private:
	vr::ITrackedDeviceServerDriver* _controller;

	public:
	ControllerVTableInfo(vr::ITrackedDeviceServerDriver* controller);
	~ControllerVTableInfo();

#pragma region IVrControllerComponent Hooks
	subhook::Hook GetControllerStateHook;
	subhook::Hook TriggerHapticPulseHook;
#pragma endregion
};

class DeviceInfo
{
	private:
	std::map<void**, std::unique_ptr<ControllerVTableInfo>> _componentMap;

	public:
	vr::ITrackedDeviceServerDriver* driver;
	std::string serialNumber;
	uint32_t index;
	vr::ETrackedDeviceClass classType;

	DeviceInfo(vr::ITrackedDeviceServerDriver* deviceDriver, const char* deviceSerial, vr::ETrackedDeviceClass deviceClass)
		: driver(deviceDriver), serialNumber(deviceSerial), classType(deviceClass), index(-1)
	{
	}
};

class DeviceVTableInfo
{
	private:
	std::list<std::unique_ptr<DeviceInfo>> _devices;
	void** vTable;

	public:
	DeviceVTableInfo(vr::ITrackedDeviceServerDriver* deviceDriver);
	~DeviceVTableInfo();
	void AddDevice(vr::ITrackedDeviceServerDriver* deviceDriver, const char* deviceSerial, vr::ETrackedDeviceClass deviceClass);
	void RemoveDevice(vr::ITrackedDeviceServerDriver* deviceDriver);
	void UpdateNextIndex(uint32_t deviceIndex);
	std::unique_ptr<DeviceInfo>& GetDevice(uint32_t deviceIndex);

#pragma region ITrackedDeviceServerDriver Hooks
	subhook::Hook ActivateHook;
	subhook::Hook DeactivateHook;
	subhook::Hook EnterStandbyHook;
	subhook::Hook GetComponentHook;
	subhook::Hook DebugRequestHook;
	subhook::Hook GetPoseHook;
#pragma endregion
};

struct OmniHookActions
{
	Action<vr::IVRServerDriverHost*, const char*, vr::ETrackedDeviceClass, vr::ITrackedDeviceServerDriver *> TrackedDeviceAdded;
	Action<vr::IVRServerDriverHost*, uint32_t, vr::DriverPose_t *, uint32_t> TrackedDevicePoseUpdated;
	Action<vr::IVRServerDriverHost*, double> VsyncEvent;
	Action<vr::IVRServerDriverHost*, std::string, uint32_t, vr::EVREventType, const vr::VREvent_Data_t&, double> VendorSpecificEvent;
	Action<vr::IVRServerDriverHost*> IsExiting;
	Action<vr::IVRServerDriverHost*, vr::VREvent_t*, uint32_t> PollNextEvent;
	Action<vr::IVRServerDriverHost*, float, vr::TrackedDevicePose_t*, uint32_t> GetRawTrackedDevicePoses;
	Action<vr::IVRServerDriverHost*, std::string, uint32_t, vr::HmdMatrix34_t, vr::HmdMatrix34_t> TrackedDeviceDisplayTransformUpdated;


	Action<vr::ITrackedDeviceServerDriver*, std::string, uint32_t, DeviceVTableInfo*> Activate;

	Action<vr::ITrackedDeviceServerDriver*> Deactivate;
	Action<vr::ITrackedDeviceServerDriver*> EnterStandby;
	Action<vr::ITrackedDeviceServerDriver*, const char *> GetComponent;
	Action<vr::ITrackedDeviceServerDriver*, const char *, char *, uint32_t> DebugRequest;
	Action<vr::ITrackedDeviceServerDriver*, subhook::Hook*, vr::DriverPose_t*> GetPose;
};

class OmniHookManager
{
	public:

	private:
	static OmniHookManager* _instance;
	vr::IVRServerDriverHost* _server;
	std::unique_ptr<OmniHookActions> _actions;
	std::map<void**, std::unique_ptr<DeviceVTableInfo>> _driverMap;

	public:
	OmniHookManager(vr::IVRServerDriverHost* server, std::unique_ptr<OmniHookActions>& actions);
	std::unique_ptr<DeviceInfo>& GetDevice(uint32_t deviceIndex);
	subhook::Hook* GetPoseHook(vr::ITrackedDeviceServerDriver* deviceDriver);
	~OmniHookManager();
	static OmniHookManager* GetInstance();
	static void Cleanup();


#pragma region IVrServerDriverHost Hooks
	subhook::Hook TrackedDeviceAddedHook;
	subhook::Hook TrackedDevicePoseUpdatedHook;
	subhook::Hook VsyncEventHook;
	subhook::Hook VendorSpecificEventHook;
	subhook::Hook IsExitingHook;
	subhook::Hook PollNextEventHook;
	subhook::Hook GetRawTrackedDevicePosesHook;
	subhook::Hook TrackedDeviceDisplayTransformUpdatedHook;
#pragma endregion


#pragma region IVrServerDriverHost
	/** Notifies the server that a tracked device has been added. If this function returns true
	* the server will call Activate on the device. If it returns false some kind of error
	* has occurred and the device will not be activated. */
	static bool TrackedDeviceAdded(vr::IVRServerDriverHost* server, const char* deviceSerial, vr::ETrackedDeviceClass deviceClass, vr::ITrackedDeviceServerDriver *deviceDriver);

	/** Notifies the server that a tracked device's pose has been updated */
	static void TrackedDevicePoseUpdated(vr::IVRServerDriverHost* server, uint32_t deviceIndex, const vr::DriverPose_t & pose, uint32_t poseSize);

	/** Notifies the server that vsync has occurred on the the display attached to the device. This is
	* only permitted on devices of the HMD class. */
	static void VsyncEvent(vr::IVRServerDriverHost* server, double vsyncTimeOffsetSeconds);

	/** Sends a vendor specific event (vr::IVRServerDriverHost* server, VREvent_VendorSpecific_Reserved_Start..VREvent_VendorSpecific_Reserved_End */
	static void VendorSpecificEvent(vr::IVRServerDriverHost* server, uint32_t deviceIndex, vr::EVREventType eventType, const vr::VREvent_Data_t & eventData, double eventTimeOffset);

	/** Returns true if SteamVR is exiting */
	static bool IsExiting(vr::IVRServerDriverHost* server);

	/** Returns true and fills the event with the next event on the queue if there is one. If there are no events
	* this method returns false. uncbVREvent should be the size in bytes of the VREvent_t struct */
	static bool PollNextEvent(vr::IVRServerDriverHost* server, vr::VREvent_t *event, uint32_t uncbVREvent);

	/** Provides access to device poses for drivers.  Poses are in their "raw" tracking space which is uniquely
	* defined by each driver providing poses for its devices.  It is up to clients of this function to correlate
	* poses across different drivers.  Poses are indexed by their device id, and their associated driver and
	* other properties can be looked up via IVRProperties. */
	static void GetRawTrackedDevicePoses(vr::IVRServerDriverHost* server, float secondsFromNow, vr::TrackedDevicePose_t *devicePoseArray, uint32_t devicePoseArrayCount);

	/** Notifies the server that a tracked device's display component transforms have been updated. */
	static void TrackedDeviceDisplayTransformUpdated(vr::IVRServerDriverHost* server, uint32_t deviceIndex, vr::HmdMatrix34_t eyeToHeadLeft, vr::HmdMatrix34_t eyeToHeadRight);
#pragma endregion

#pragma region ITrackedDeviceServerDriver
	// ------------------------------------
	// Management Methods
	// ------------------------------------
	/** This is called before an HMD is returned to the application. It will always be
	* called before any display or tracking methods. Memory and processor use by the
	* ITrackedDeviceServerDriver object should be kept to a minimum until it is activated.
	* The pose listener is guaranteed to be valid until Deactivate is called, but
	* should not be used after that point. */
	static vr::EVRInitError Activate(vr::ITrackedDeviceServerDriver* driver, uint32_t unObjectId);

	/** This is called when The VR system is switching from this Hmd being the active display
	* to another Hmd being the active display. The driver should clean whatever memory
	* and thread use it can when it is deactivated */
	static void Deactivate(vr::ITrackedDeviceServerDriver* driver);

	/** Handles a request from the system to put this device into standby mode. What that means is defined per-device. */
	static void EnterStandby(vr::ITrackedDeviceServerDriver* driver);

	/** Requests a component interface of the driver for device-specific functionality. The driver should return NULL
	* if the requested interface or version is not supported. */
	static void* GetComponent(vr::ITrackedDeviceServerDriver* driver, const char *pchComponentNameAndVersion);

	/** A VR Client has made this debug request of the driver. The set of valid requests is entirely
	* up to the driver and the client to figure out, as is the format of the response. Responses that
	* exceed the length of the supplied buffer should be truncated and null terminated */
	static void DebugRequest(vr::ITrackedDeviceServerDriver* driver, const char *pchRequest, char *pchResponseBuffer, uint32_t unResponseBufferSize);

	// ------------------------------------
	// Tracking Methods
	// ------------------------------------
	//static vr::DriverPose_t GetPose(vr::ITrackedDeviceServerDriver* driver);
	static vr::DriverPose_t GetPose();
#pragma endregion
};

