#include "InputManager.h"
#include "OmniLog.h"
#include "openvr_math.h"

#pragma comment (lib, "Setupapi.lib")
#pragma comment (lib, "Advapi32.lib")


InputManager::InputManager()
{
}

InputManager::~InputManager()
{

}


float ReadRegistryValue(const std::wstring& subKey, const std::wstring& value)
{
	HKEY keyHandle = nullptr;
	LONG retCode = ::RegOpenKey(HKEY_CURRENT_USER, subKey.c_str(), (PHKEY)&keyHandle);

	TCHAR data[MAXIMUM_REPARSE_DATA_BUFFER_SIZE];
	DWORD dataSize = sizeof(data);
	RegQueryValueEx(keyHandle, value.c_str(), nullptr, nullptr, reinterpret_cast<LPBYTE>(data), &dataSize);

	return _wtof(data);
}

bool InputManager::GetOmniInput(vr::HmdVector3d_t& axis, float& yaw)
{
	if(!OmniConnected())
	{
		ConnectToOmni();
	}

	if(OmniConnected())
	{
		float xAxis;
		float yAxis;
		float OmniTimeStamp;

		unsigned char OmniInputBuffer[65] = {0};
		int iTimeArray[4] = {0};
		bool inputRecieved = false;

		int iCurrentStepCount;
		MotionData motionData;

		xAxis = 0.f;
		yAxis = 0.f;
		yaw = 0.f;
		OmniTimeStamp = 0.f;

		OmniInputBuffer[0] = 0;

		INT32 readResult = hid_read(omniDevice, OmniInputBuffer, 65);

		if(readResult == -1)
		{
			//OmniLog::AppendLine("Omni is NOT receiving Input! Please make sure the Omni is connected and powered on!");
			return false;
		}


		//Data is not valid
		if((OmniInputBuffer[0] != 0xEF) || (OmniInputBuffer[2] != 0xA9))	//Checking for Invalid Message
		{
			//OmniLog::AppendLine("Failed to Read HID Data.");

			return false;
		}

		/* PAYLOAD DEFINITION:
		* Start at index 5 for most things:
		byte[0-3] = Timestamp (uint32)
		byte[4-7] = Step Count (uint32)
		byte[8-11] = Ring Angle (float32)
		byte[12] = Ring Delta (1 byte)
		byte[13-14] = Gamepad Data (2 bytes)
		byte[15] = L/R Step Trigger (1 byte)
		byte[16-23] = Pod 1 Quaternions (8 bytes, 2 bytes per)
		byte[24-29] = Pod 1 Accelerometer (6 bytes, 2 bytes per)
		byte[30-35] = Pod 1 Gyroscope (6 bytes, 2 bytes per)
		byte[36-43] = Pod 2 Quaternions (8 bytes, 2 bytes per)
		byte[44-49] = Pod 2 Accelerometer (6 bytes, 2 bytes per)
		byte[50-55] = Pod 2 Gyroscope (6 bytes, 2 bytes per)
		*/

		motionData.Timestamp = *(int *)&OmniInputBuffer[5 + 0];
		motionData.StepCount = *(int *)&OmniInputBuffer[5 + 4];

		ByteFloatUnion ringAngle;
		ringAngle.b[0] = OmniInputBuffer[5 + 8];
		ringAngle.b[1] = OmniInputBuffer[5 + 9];
		ringAngle.b[2] = OmniInputBuffer[5 + 10];
		ringAngle.b[3] = OmniInputBuffer[5 + 11];

		motionData.RingAngle = ringAngle.f;

		motionData.RingDelta = OmniInputBuffer[5 + 12];
		motionData.GameX = OmniInputBuffer[5 + 13];
		motionData.GameY = OmniInputBuffer[5 + 14];

		//if(iCurrentTimeArrayIdx < 3)
		//{
		//	iTimeArray[iCurrentTimeArrayIdx] = Omni_Internal_Motion.Timestamp;
		//	iCurrentTimeArrayIdx++;
		//}
		//else
		{
			iTimeArray[0] = iTimeArray[1];
			iTimeArray[1] = iTimeArray[2];
			iTimeArray[2] = iTimeArray[3];
			iTimeArray[3] = motionData.Timestamp;
		}

		//if(iCurrentTimeArrayIdx == 3)
		{
			if((iTimeArray[0] == iTimeArray[1])
				&& (iTimeArray[1] == iTimeArray[2])
				&& (iTimeArray[2] == iTimeArray[3]))
				inputRecieved = false;
			else
				inputRecieved = true;
		}

		//if(bResetStepCount && bIsReceivingInput)
		//{
		//	OmniLog::AppendLine("Internal Stepcount = %i", Omni_Internal_Motion.StepCount);
		//	//iStartingStepCount = Omni_Internal_Motion.StepCount;
		//	//iCurrentStepCount = 0;
		//	//bResetStepCount = false;
		//}


		if(inputRecieved)
		{
			xAxis = motionData.GameX;
			yAxis = motionData.GameY;
			iCurrentStepCount = motionData.StepCount - 0;

			yaw = motionData.RingAngle;
			OmniTimeStamp = motionData.Timestamp;

			if(xAxis == 127)
			{
				xAxis = 0;
			}
			else
			{
				if(xAxis > 0.0)
				{
					xAxis = (xAxis / 255.0) * 2.0 - 1.0;
				}
				else
				{
					xAxis = -1.0;
				}
			}

			if(yAxis == 127)
			{
				yAxis = 0;
			}
			else
			{
				if(yAxis > 0.0)
				{
					yAxis = (yAxis / 255.0) * 2.0 - 1.0;
				}
				else
				{
					yAxis = -1.0;
				}
			}

			yAxis *= -1.0;

			//if(yaw > 360.0)
			//	yaw -= 360.0;
			//else if(yaw < 0.0)
			//	yaw += 360.0;

			axis.v[0] = xAxis;
			axis.v[1] = 0;
			axis.v[2] = yAxis;


			//float CouplingPercentage = stof(ReadRegistryValue("Software\\HeroVR\\SDK\\CouplingPercentage", ""));
			
			float OmniYawOffset = 40;
			OmniYawOffset = ReadRegistryValue(L"Software\\HeroVR\\SDK\\OmniYawOffset", L"");
			yaw = vrmath::deg2rad(yaw + OmniYawOffset);

			//OmniLog::AppendLine("OmniYaw(%f) RawYaw(%f) OmniOffset(%f) OmniAxis(%f, %f) ", yaw, motionData.RingAngle, OmniYawOffset, xAxis, yAxis);

			//vr::HmdQuaternion_t rotation = vrmath::QuaternionFromEuler(vrmath::PI * 0.5f, yaw, 0);
			//vr::HmdVector3d_t origEuler = {90, 90, 0};
			//auto rotation = vrmath::QuaternionFromDegrees(origEuler.x, origEuler.y, origEuler.z);
			//vr::HmdVector3d_t forward = {0, 0, 1};
			//auto rotated = vrmath::QuaternionRotateVector(rotation, forward);
			//OmniLog::AppendLine("foward(%f, %f, %f) rotated(%f, %f, %f)", forward.x, forward.y, forward.z, rotated.x, rotated.y, rotated.z);
			//auto euler = vrmath::DegreesFromQuaternion(rotation);
			//OmniLog::AppendLine("orig(%i, %i, %i) euler(%i, %i, %i)", (int)origEuler.pitch, (int)origEuler.yaw, (int)origEuler.roll, (int)euler.pitch, (int)euler.yaw, (int)euler.roll);

			return true;
		}
	}
	return false;
}

bool InputManager::OmniConnected()
{
	return omniDevice != nullptr;
}

void InputManager::ConnectToOmni()
{
	bool deviceFound = false;

	hid_device_info *devs = nullptr;
	hid_device_info *cur_dev = nullptr;

	std::wstring serialNumber;
	std::string path;
	devs = hid_enumerate(0x0, 0x0);
	cur_dev = devs;

	if(devs != nullptr)
	{
		if(IsDeviceAnOmni(*cur_dev, cur_dev->path, "mi_04"))
		{
			serialNumber = std::wstring(cur_dev->serial_number);
			path = std::string(cur_dev->path);
			omniInfo = cur_dev;

			omniDevice = hid_open_path(cur_dev->path);
			deviceFound = true;
		}
		else
		{
			devs = hid_enumerate(0x0, 0x0);
			cur_dev = devs;

			if(IsDeviceAnOmni(*cur_dev, cur_dev->path, "mi_00"))
			{
				serialNumber = cur_dev->serial_number;
				path = cur_dev->path;
				omniInfo = cur_dev;

				omniDevice = hid_open_path(cur_dev->path);
				deviceFound = true;
			}
		}

		if(deviceFound)
		{
			hid_free_enumeration(devs);
			hid_set_nonblocking(omniDevice, 1);
		}

		if(omniDevice != nullptr)
		{
			OmniLog::AppendLine("Omni device found %p", omniDevice);
		}
	}
}



bool InputManager::IsDeviceAnOmni(hid_device_info& cur_dev, char *path, const char *OmniType)
{
	for(; *path; ++path)
	{
		*path = tolower(*path);
	}

	while(true)
	{
		path = strstr(cur_dev.path, "vid_29eb");

		if(path != NULL)
		{
			path = strstr(cur_dev.path, OmniType);

			if(path != NULL)
				return true;
		}

		if(cur_dev.next)
		{
			cur_dev = *cur_dev.next;
		}
		else
		{
			break;
		}
	}

	return false;
}

InputManager& InputManager::GetInstance()
{
	static InputManager instance;
	return instance;
}
