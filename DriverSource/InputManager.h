#pragma once
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <Windows.h>
#include <openvr_driver.h>
#include "hidapi.h"



struct MotionData
{
	int Timestamp;
	int StepCount;
	float RingAngle = -1;
	unsigned char RingDelta;
	unsigned char GameX;
	unsigned char GameY;
	unsigned char GunButtons;
	unsigned char StepTrigger;
};

union ByteFloatUnion
{
	float f;
	unsigned char b[4];
};

class InputManager
{
	public:
	static InputManager& GetInstance();
	bool GetOmniInput(vr::HmdVector3d_t& axis, float& rotation);
	bool OmniConnected();
	void ConnectToOmni();

	//static float SqrMagnitude(const vr::VRControllerAxis_t& vec)
	//{
	//	return vec.x * vec.x + vec.y * vec.y;
	//}

	//static float Magnitude(const vr::VRControllerAxis_t& vec)
	//{
	//	return sqrt(SqrMagnitude(vec));
	//}

	//static float Clamp1M1(const float value)
	//{
	//	if(value > 1)
	//		return 1;
	//	if(value < -1)
	//		return -1;
	//	return value;
	//}


	private:
	InputManager();
	~InputManager();

	static bool IsDeviceAnOmni(hid_device_info& cur_dev, char *path, const char *OmniType);

	hid_device_info* omniInfo = nullptr;
	hid_device* omniDevice = nullptr;
};

